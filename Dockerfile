# base image
FROM node:10.4.0-alpine

# set working directory
RUN mkdir /usr/src
WORKDIR /usr/src

COPY . ./
RUN npm install
RUN npm rebuild node-sass

# start app
CMD ["npm", "start"]